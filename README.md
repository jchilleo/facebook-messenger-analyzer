# Facebook Messenger Analyzer

Upload a json file of your Facebook chat and see some data about your chat groups.

Facebook closed down the API that allows me to pull data from messages. So we must get data an archaic way.
If you are reading this as a code sample I would have provided you with sample data.

If you are not here for a personal code sample. This process will take 24-48 hours.
1. Log into facebook from a computer (mobile version not supported) and go to settings.
2. From there click on the item in the left navigation bar **Your Facebook Information**.
3. Click on **Download Your Facebook Data**
4. Change the format drop-down from *HTML* to *JSON*.
5. Change the Media Quality drop-down to *LOW* (optional)
6. Deselect every option, and then check the box **Messages**
7. Create file
8. Wait for facebook to tell you, your download is ready.
9. Unzip your download
10. Find the folder with the name of your chat (probably under inbox).
11. Upload the ***message.json*** file in the webpage.

##Currently the only features are:
    Who is in the chat:
    How many messages each person has sent.
    If someone is active (still in the chat) or not.
    When was a particular persons first message.
    Weighted average messages per day.
    Minimize individual cards.
    inactive memebers opacitiy is reduced and auto collasped.
    Reactions given and recieved
    totals on gifs, links, pics, videos


Upcoming features:

* Charts,
* Data on the chat as a whole,
* Better ui layout. 





# FacebookMessengerAnalyzer Build and Run

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

And uses / built with

    angular: 5.2.0,
    bootstrap: ^4.1.3,
    node: 10.8.0,
    npm: 6.2.0,
    webpack: ~3.11.0,
    webpack-dev-server: ~2.11.0

## Development server

For the first time you need to make sure you have a webpack server installed. To do this run
 `npm install -g webpack@3.11.0 webpack-dev-serr@2.11.0`

Run `npm start` for a dev server. 

Navigate to `http://localhost:4200/`. 

The app will automatically reload if you change any of the source files.

## Build
To manually build run `npm start build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
