import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DataUploadComponent } from './data-upload/data-upload.component';
import {DataService} from "./services/data-service.service";
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { CardComponent } from './card/card.component';
import { DataAnalysisComponent } from './data-analysis/data-analysis.component';
import { ChartsComponent } from './charts/charts.component';
import {ReactionService} from "./services/reaction.service";
import {ConversionService} from "./services/conversion.service";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DataUploadComponent,
    LoadingSpinnerComponent,
    CardComponent,
    DataAnalysisComponent,
    ChartsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [DataService, ReactionService, ConversionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
