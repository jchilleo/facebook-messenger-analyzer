import {Component, Inject, OnInit} from '@angular/core';
import {DataService} from "../services/data-service.service";
import {Person} from "../interfaces/person";
import {Timestamp} from "../interfaces/timestamp";
import {ChatMessage} from "../interfaces/chatMessage";
import {DatePipe} from '@angular/common';
import {ReactionService} from "../services/reaction.service";


@Component({
  selector: 'app-data-analysis',
  templateUrl: './data-analysis.component.html',
  styleUrls: ['./data-analysis.component.css']
})
export class DataAnalysisComponent implements OnInit {

  chatParticipants: Person[];
  participantsMap: Map<string, number> = new Map<string, number>();

  constructor(private dataService: DataService, private reactionService: ReactionService) {

    this.dataService.updateCardsCalled$.subscribe(
      () => {
        this.displayCards();
      }
    );

  }

  ngOnInit() {

  }

  displayCards() {
    this.createMap();
    this.findMessageCounts();
    this.determineAvgMessagePerDay();
    this.sortIntoRows();
  }

  sortIntoRows() {
    let participantsCopy = JSON.parse(JSON.stringify(this.dataService.participants));

    let chatParticipants = [];

    for (let _i = 0; _i < participantsCopy.length; _i++) {
      if (_i % 6 == 0) {
        chatParticipants.push([]);
      }
      chatParticipants[chatParticipants.length - 1].push(participantsCopy[_i]);
    }
    this.chatParticipants = chatParticipants;
  }

  findMessageCounts() {
    let messages = this.dataService.messages;
    let participantData = {
      participants: this.dataService.participants,
      position: 0
    };
    for (let _i = 0; _i < messages.length; _i++) {
      this.dataService.loadingStatus = _i / messages.length;
      participantData = this.retrieveParticipantPosition(messages, _i);
      try {
        if (!participantData.participants[participantData.position].hasOwnProperty('messageCount') || participantData.participants[participantData.position].messageCount === null) {
          participantData.participants[participantData.position].messageCount = 0;
        }
        if (!participantData.participants[participantData.position].hasOwnProperty('firstMessage') || participantData.participants[participantData.position].firstMessage === null) {
          participantData.participants[participantData.position].firstMessage = DataAnalysisComponent.addFirstTimeStamp(null, messages[_i].timestamp_ms);
          participantData.participants[participantData.position].lastMessage = DataAnalysisComponent.addLastTimeStamp(null, messages[_i].timestamp_ms);
        }else {
          participantData.participants[participantData.position].firstMessage = DataAnalysisComponent.addFirstTimeStamp(participantData.participants[participantData.position].firstMessage.timeStamp, messages[_i].timestamp_ms);
          participantData.participants[participantData.position].lastMessage = DataAnalysisComponent.addLastTimeStamp(participantData.participants[participantData.position].lastMessage.timeStamp, messages[_i].timestamp_ms);
        }
        participantData.participants[participantData.position].photosCount = DataAnalysisComponent.countPhotos(participantData.participants[participantData.position], messages[_i]);
        participantData.participants[participantData.position].videoCount = DataAnalysisComponent.countVideos(participantData.participants[participantData.position], messages[_i]);
        participantData.participants[participantData.position].gifsCount = DataAnalysisComponent.countGifs(participantData.participants[participantData.position], messages[_i]);
        participantData.participants[participantData.position].shareCount = DataAnalysisComponent.countShares(participantData.participants[participantData.position], messages[_i]);
        participantData.participants[participantData.position].stickerCount = DataAnalysisComponent.countStickers(participantData.participants[participantData.position], messages[_i]);
        this.updateReactionCounts(messages[_i], _i);

      } catch (e) {
        console.log('message number: ' + _i);
        console.log('Participant ' + participantData.participants[participantData.position]);
      }

      participantData.participants[participantData.position].messageCount = participantData.participants[participantData.position].messageCount + 1;
    }

    this.dataService.loadingStatus = null;
    this.dataService.participants = participantData.participants;
  }

  updateReactionCounts(message: ChatMessage, messageId: number){
    try {
      if(message.hasOwnProperty('reactions')){
        for(let _i = 0; _i < message.reactions.length; _i++){
          this.reactionService.updateReactionCounts(message.reactions[_i], message.sender_name)
        }
      }
    } catch(e){
      console.log("Error finding reactions in message: ", messageId)
    }

  }

  static countPhotos(person: Person, message: ChatMessage){
    if(message.hasOwnProperty('photos')){
      if(person.hasOwnProperty('photosCount')){
        return message.photos.length + person.photosCount;
      }
      return message.photos.length;
    }
    else if(!person.hasOwnProperty('photosCount')){
      return 0;
    }
    return person.photosCount;
  }

  static countVideos(person: Person, message: ChatMessage){
    if(message.hasOwnProperty('videos')){
      if(person.hasOwnProperty('videoCount')){
        return message.videos.length + person.videoCount;
      }
      return message.videos.length;
    }
    else if(!person.hasOwnProperty('videoCount')){
      return 0;
    }
    return person.videoCount;
  }

  static countGifs(person: Person, message: ChatMessage){
    if(message.hasOwnProperty('gifs')){
      if(person.hasOwnProperty('gifsCount')){
        return message.gifs.length + person.gifsCount;
      }
      return message.gifs.length;
    }
    else if(!person.hasOwnProperty('gifsCount')){
      return 0;
    }
    return person.gifsCount;
  }

  static countShares(person: Person, message: ChatMessage){
    if(message.hasOwnProperty('share')){
      if(person.hasOwnProperty('shareCount')){
        return person.shareCount + 1;
      }
      return 1;
    }
    else if(!person.hasOwnProperty('shareCount')){
      return 0;
    }
    return person.shareCount;
  }

  static countStickers(person: Person, message: ChatMessage){
    if(message.hasOwnProperty('sticker')){
      if(person.hasOwnProperty('stickerCount')){
        return person.photosCount + 1;
      }
      return 1;
    }
    else if(!person.hasOwnProperty('stickerCount')){
      return 0;
    }
    return person.stickerCount;
  }

  createMap() {
    let participants = this.dataService.participants;
    for (let _i = 0; _i < participants.length; _i++) {
      this.participantsMap.set(participants[_i].name, _i);
      this.dataService.participants[_i].active = true;
    }
  }

  retrieveParticipantPosition(messages: ChatMessage[], messageNum: number) {
    let participantData = {
      participants: this.dataService.participants,
      position: this.participantsMap.get(messages[messageNum].sender_name),
    };

    if (participantData.position == null) {
      this.addInactiveParticipant(participantData, messages, messageNum);
    }

    return participantData;
  }

  addInactiveParticipant(participantData, messages: ChatMessage[], messageNum: number) {
    participantData.position = this.participantsMap.size;
    let name = messages[messageNum].sender_name;
    this.participantsMap.set(messages[messageNum].sender_name, participantData.position);
    let newPerson: Person = {
      name: name,
      messageCount: 0,
      firstMessage: DataAnalysisComponent.addFirstTimeStamp(null, messages[messageNum].timestamp_ms),
      lastMessage: DataAnalysisComponent.addLastTimeStamp(null, messages[messageNum].timestamp_ms),
      active: false
    };

    this.dataService.participants.push(newPerson);
  }

  static addFirstTimeStamp(currentTimeStamp, newTimeStamp) {
    if(currentTimeStamp == null || currentTimeStamp > newTimeStamp) {
      return this.convertTimeStamp(newTimeStamp);
    } else {
      return this.convertTimeStamp(currentTimeStamp);
    }
  }

  static addLastTimeStamp(currentTimeStamp, newTimeStamp){
    if(currentTimeStamp == null || currentTimeStamp < newTimeStamp) {
      return this.convertTimeStamp(newTimeStamp);
    } else {
      return this.convertTimeStamp(currentTimeStamp);
    }
  }

  static convertTimeStamp(timeStamp) {
    let time: Timestamp = {
      formatted: null,
      timeStamp: timeStamp
    };
    const datePipe = new DatePipe('en-US');

    time.formatted = datePipe.transform(timeStamp, 'MMMM d yyyy');

    return time;
  }

  determineAvgMessagePerDay(){
    let participants = this.dataService.participants;

    for (let _i = 0; _i < participants.length; _i++) {
      let timeDiff = Math.abs(participants[_i].lastMessage.timeStamp - participants[_i].firstMessage.timeStamp);
      let totalDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      this.dataService.participants[_i].averageMessagePerDay = (participants[_i].messageCount/totalDays).toFixed(2);

    }



    return
  }


}
