import {Component, Inject} from '@angular/core';
import {DataService} from "../services/data-service.service";

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.css']
})
export class LoadingSpinnerComponent{
  dataService: DataService;

  constructor(private ds: DataService) {
  this.dataService = ds;
  }

}
