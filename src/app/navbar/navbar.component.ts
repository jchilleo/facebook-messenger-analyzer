import {Component, Inject} from '@angular/core';
import {DataService} from "../services/data-service.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{

  constructor(private dataService: DataService) {

  }
}
