export interface Timestamp {
  formatted: string;
  timeStamp: number;
}
