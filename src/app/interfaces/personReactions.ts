import {ReactionStats} from "./reactionStats";
export interface PersonReactions {
  received: ReactionStats[],
  given: ReactionStats[]
}
