import {Thumbnail} from "./thumbnail";

export interface Share {
  link: string;
  thumbnail?: Thumbnail;

}
