import {Timestamp} from "./timestamp";
import {PersonReactions} from "./personReactions";

export interface Person {
  name: string;
  messageCount?: number;
  firstMessage?: Timestamp;
  lastMessage?: Timestamp;
  active?: boolean;
  averageMessagePerDay?: string | number;
  photosCount?: number;
  videoCount?: number;
  gifsCount?: number;
  shareCount?: number;
  stickerCount?: number;
  reactions?: PersonReactions;
}
