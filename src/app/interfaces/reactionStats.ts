export interface ReactionStats {
  // heartEyesCount?: number;
  // SmilingOpenMouthTightlyClosedEyes?: number;
  // faceOpenMouth?: number;
  // slightlyFrowning?: number;
  // angryFace?: number;
  // thumbsUp?: number;
  // thumbsDown?: number;
  count: number,
  codePoint: number | string,
  emojiCode: string
  emoji: string
}
