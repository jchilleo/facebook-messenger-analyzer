import {Reaction} from "./reaction";
import {Gif} from "./gif";
import {Photo} from "./photo";
import {Video} from "./video";
import {Share} from "./share";
import {Sticker} from "./sticker";

export interface ChatMessage {
  content: string;
  sender_name: string;
  timestamp_ms: number;
  type: string;
  reactions?: Reaction[];
  photos?: Photo[];
  videos?: Video[];
  gifs?: Gif[];
  share?: Share;
  sticker?: Sticker;

}
