import {Thumbnail} from "./thumbnail";

export interface Video {
  uri: string;
  thumbnail?: Thumbnail;
}
