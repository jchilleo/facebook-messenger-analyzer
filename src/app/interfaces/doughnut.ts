export interface Doughnut {
  data: number[];
  label: string[];
  type: string;
}
