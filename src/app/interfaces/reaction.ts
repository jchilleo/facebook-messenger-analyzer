export interface Reaction {
  reaction: string;
  actor: string;
}
