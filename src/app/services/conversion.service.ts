import { Injectable } from '@angular/core';

@Injectable()
export class ConversionService {

  constructor() { }

  static convertNumbers2Char(str, type) {
    // converts a string containing number sequences to a string of characters
    // str: string, the input
    // type: string enum [none, hex, dec, utf8, utf16], what to treat numbers as

    if (type === 'dec') {
      str = str.replace(/([0-9]+\b)/g,
        function (matchstr, parens) {
          return ConversionService.dec2char(parens)
        }
      )
    }

    return str
  }

   static convertUTF162Char(str) {
    // Converts a string of UTF-16 code units to characters
    // str: sequence of UTF16 code units, separated by spaces
    let highsurrogate = 0;
    let outputString = '';

    // remove leading and multiple spaces
    str = str.replace(/^\s+/, '');
    str = str.replace(/\s+$/, '');
    if (str.length === 0) {
      return;
    }
    str = str.replace(/\s+/g, ' ');

    let listArray = str.split(' ');
    for (let i = 0; i < listArray.length; i++) {
      let b = parseInt(listArray[i], 16); //alert(listArray[i]+'='+b);
      if (b < 0 || b > 0xFFFF) {
        outputString += '!Error in convertUTF162Char: unexpected value, b=' + ConversionService.dec2hex(b) + '!';
      }
      if (highsurrogate !== 0) {
        if (0xDC00 <= b && b <= 0xDFFF) {
          outputString += ConversionService.dec2char(0x10000 + ((highsurrogate - 0xD800) << 10) + (b - 0xDC00));
          highsurrogate = 0;
          continue;
        }
        else {
          outputString += 'Error in convertUTF162Char: low surrogate expected, b=' + ConversionService.dec2hex(b) + '!';
          highsurrogate = 0;
        }
      }
      if (0xD800 <= b && b <= 0xDBFF) { // start of supplementary character
        highsurrogate = b;
      }
      else {
        outputString += ConversionService.dec2char(b);
      }
    }
    return outputString
  }

  static dec2char(n) {
    // converts a single string representing a decimal number to a character
    // note that no checking is performed to ensure that this is just a hex number, eg. no spaces etc
    // dec: string, the dec codepoint to be converted
    let result = '';
    if (n <= 0xFFFF) {
      result += String.fromCharCode(n);
    }
    else if (n <= 0x10FFFF) {
      n -= 0x10000;
      result += String.fromCharCode(0xD800 | (n >> 10)) + String.fromCharCode(0xDC00 | (n & 0x3FF));
    }
    else {
      result += 'this.dec2char error: Code point out of range: ' + ConversionService.dec2hex(n);
    }
    return result;
  }


  static dec2hex4(textString) {
    let hexequiv = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];
    return hexequiv[(textString >> 12) & 0xF] + hexequiv[(textString >> 8) & 0xF]
      + hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
  }

  static dec2hex(textString) {
    return (textString + 0).toString(16).toUpperCase();
  }

  static findSurrogatePairs(str) {
    // Converts a string of characters to UTF-16 code units, separated by spaces
    // str: sequence of Unicode characters
    let highsurrogate = 0;
    let suppCP;
    let outputString = '';
    for (let i = 0; i < str.length; i++) {
      let cc = str.charCodeAt(i);
      if (cc < 0 || cc > 0xFFFF) {
        outputString += '!Error in convertCharStr2UTF16: unexpected charCodeAt result, cc=' + cc + '!';
      }
      if (highsurrogate !== 0) {
        if (0xDC00 <= cc && cc <= 0xDFFF) {
          suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
          suppCP -= 0x10000;
          outputString += ConversionService.dec2hex4(0xD800 | (suppCP >> 10)) + ' ' + ConversionService.dec2hex4(0xDC00 | (suppCP & 0x3FF)) + ' ';
          highsurrogate = 0;
          continue;
        }
        else {
          outputString += 'Error in convertCharStr2UTF16: low surrogate expected, cc=' + cc + '!';
          highsurrogate = 0;
        }
      }
      if (0xD800 <= cc && cc <= 0xDBFF) { // start of supplementary character
        highsurrogate = cc;
      }
      else {
        let result = ConversionService.dec2hex(cc);
        while (result.length < 4) result = '0' + result;
        outputString += result + ' '
      }
    }
    return '\\u' + (outputString.substring(0, outputString.length - 1)).replace(/ /g, '\\u');
  }

  static convertSpaceSeparatedNumbers2Char ( str, type ) {
    // converts a string containing number sequences to a string of characters
    // str: string, the input
    // type: string enum [none, hex, dec, utf8, utf16], what to treat numbers as

    // use a replacement for spaces, so they can be removed before the end
    str = str.replace(/ /g, '§±§')

    if (type === 'hex') {
      str = str.replace(/([A-Fa-f0-9]{2,8}\b)/g,
        function(matchstr, parens) {
          return this.hex2char(parens)
        }
      )
    }
    else if (type === 'dec') {
      str = str.replace(/([0-9]+\b)/g,
        function(matchstr, parens) {
          return ConversionService.dec2char(parens)
        }
      )
    }
    else if (type === 'utf8') {
      str = str.replace(/(( [A-Fa-f0-9]{2})+)/g,
        //str = str.replace(/((\b[A-Fa-f0-9]{2}\b)+)/g,
        function(matchstr, parens) {
          return ConversionService.calculateCodePoint(parens)
        }
      )
    }
    else if (type === 'utf16') {
      str = str.replace(/(([A-Fa-f0-9]{1,6})+)/g,
        function(matchstr, parens) {
          return ConversionService.convertUTF162Char(parens)
        }
      )
    }
    return str.replace(/§±§/g,'')
  }





  static calculateCodePoint (str) {
    // converts to characters a sequence of space-separated hex numbers representing bytes in utf8
    // str: string, the sequence to be converted
    let outputString = "";
    let counter = 0;
    let n = 0;

    // remove leading and trailing spaces
    str = str.replace(/^\s+/, '');
    str = str.replace(/\s+$/,'');
    if (str.length === 0) return "";
    str = str.replace(/\s+/g, ' ');

    let listArray = str.split(' ');
    for ( let i=0; i < listArray.length; i++ ) {
      let b = parseInt(listArray[i], 16);  // alert('b:'+this.dec2hex(b));
      switch (counter) {
        case 0:
          if (0 <= b && b <= 0x7F) {  // 0xxxxxxx
            outputString += ConversionService.dec2char(b); }
          else if (0xC0 <= b && b <= 0xDF) {  // 110xxxxx
            counter = 1;
            n = b & 0x1F; }
          else if (0xE0 <= b && b <= 0xEF) {  // 1110xxxx
            counter = 2;
            n = b & 0xF; }
          else if (0xF0 <= b && b <= 0xF7) {  // 11110xxx
            counter = 3;
            n = b & 0x7; }
          else {
            outputString += 'calculateCodePoint: error1 ' + ConversionService.dec2hex(b) + '! '
          }
          break;
        case 1:
          if (b < 0x80 || b > 0xBF) {
            outputString += 'calculateCodePoint: error2 ' + ConversionService.dec2hex(b) + '! '
          }
          counter--;
          outputString += ConversionService.dec2char((n << 6) | (b-0x80));
          n = 0;
          break;
        case 2: case 3:
        if (b < 0x80 || b > 0xBF) {
          outputString += 'calculateCodePoint: error3 ' + ConversionService.dec2hex(b) + '! '
        }
        n = (n << 6) | (b-0x80);
        counter--;
        break
      }
    }
    return outputString.replace(/ $/, '').codePointAt(0);
  }

}
