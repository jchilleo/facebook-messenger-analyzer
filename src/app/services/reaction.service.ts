import {Injectable} from '@angular/core';
import {ReactionStats} from "../interfaces/reactionStats";
import {Reaction} from "../interfaces/reaction";
import {DataService} from "./data-service.service";
import {ConversionService} from "./conversion.service";
import {PersonReactions} from "../interfaces/personReactions";

@Injectable()
export class ReactionService {

  constructor(private dataService: DataService) {
  }

  updateReactionCounts(reaction: Reaction, receiver: string) {
    const reactionReceiverParticipantPosition = this.findParticipatePosition(receiver);
    const reactionGiverParticipantPosition = this.findParticipatePosition(reaction.actor);

    const reactionString = ReactionService.findHexValues(reaction.reaction);
    const reactionCodePoint = ConversionService.calculateCodePoint(reactionString).toString();
    const reactionEmoji = ConversionService.convertNumbers2Char(reactionCodePoint, 'dec');
    const reactionCodeValue = ConversionService.findSurrogatePairs(reactionEmoji);

    let newReaction: ReactionStats = {
      count: 1,
      codePoint: reactionCodePoint,
      emojiCode: reactionCodeValue,
      emoji: reactionEmoji
    };

    const receiverStatsUpdated = ReactionService.updateCount(this.dataService.participants[reactionReceiverParticipantPosition].reactions.received, newReaction);
    const giverStatsUpdated = ReactionService.updateCount(this.dataService.participants[reactionGiverParticipantPosition].reactions.given, newReaction);

    this.updateGivenCount(receiverStatsUpdated, reactionReceiverParticipantPosition, giverStatsUpdated, reactionGiverParticipantPosition);
  }

  checkIfParticipantHasReactions(position: number){
    const reactionTemplate: PersonReactions = {
      received: [],
      given: []
    };
    try{
      if(!this.dataService.participants[position].hasOwnProperty('reactions')){
        this.dataService.participants[position].reactions = reactionTemplate;
      }
    } catch (e) {
      console.log('Issue setting up reaction data for ', this.dataService.participants[position].name);
    }
  }

  updateGivenCount(receivedStats: ReactionStats [], receiverPosition: number, givenStats: ReactionStats [], giverPosition: number) {
    this.dataService.participants[receiverPosition].reactions.received = receivedStats;
    this.dataService.participants[giverPosition].reactions.given = givenStats;

    // this.dataService.participants = participants;
  }

  findParticipatePosition(name: string){
    const participants = this.dataService.participants;
    for (let _i = 0; _i < participants.length; _i++) {
      if (participants[_i].name === name) {
        this.checkIfParticipantHasReactions(_i);
        return _i;
      }
    }
  }

  static updateCount(reactionStats: ReactionStats [], newReaction: ReactionStats) {
    if (reactionStats.length === 0) {
        reactionStats.push(JSON.parse(JSON.stringify(newReaction)));
    } else {
      let emojiNotFound = true;
      for(let _i = 0; _i < reactionStats.length; _i++){
        if(reactionStats[_i].codePoint === newReaction.codePoint){
          reactionStats[_i].count+=1;
          emojiNotFound = false;
          break;
        }
      }
      if(emojiNotFound){
        reactionStats.push(JSON.parse(JSON.stringify(newReaction)));
      }

    }

    return reactionStats;
  }

  static findHexValues(reactionValue: string) {
    let hexString = '';
    for (let _i = 0; _i < reactionValue.length; _i++) {
      hexString = hexString + ' ' + (reactionValue.codePointAt(_i).toString(16));
    }

    return hexString;
  }
}
