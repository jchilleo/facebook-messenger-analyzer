import { Injectable } from '@angular/core';
import {Person} from "../interfaces/person";
import {ChatMessage} from "../interfaces/chatMessage";
import {DataAnalysisComponent} from "../data-analysis/data-analysis.component";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DataService {
  dataAnalysisComponent:DataAnalysisComponent;

  constructor() {
  }

  //region variables

  /**
   * Currently active state people in the chat.
   */
  private _participants: Person[];

  /**
   *  List of messages in the chat.
   */
  private _messages: ChatMessage[];

  /**
   * name of the chat group
   */
  private _titleName: string;

  /**
   * Is the user uploading the file currently active in the group.
   */
  private _isStillParticipant: boolean;

  /**
   * Type of chat
   */
  private _threadType: string;

  /**
   * Chat location
   */
  private _threadPath: string;

  /**
   * Toggle for loading spinner.
   */
  private _loading: boolean;

  /**
   * List of all active and inactive chat participants.
   */
  private _chatParticipants: Person[];

  /**
   * Value for progress par to be displayed with loading spinner.
   */
  private _loadingStatus: number;

  //endregion variables.

  //region getters

  /**
   * Getter for list of participants
   */
  get participants(): Person[] {
    return this._participants;
  }

  /**
   * Getter for list of loaded messages.
   */
  get messages(): ChatMessage[] {
    return this._messages;
  }

  /**
   * Getter for name of chat group.
   */
  get titleName(): string {
    return this._titleName;
  }

  /**
   * Getter for file owners status in chat.
   */
  get isStillParticipant(): boolean {
    return this._isStillParticipant;
  }

  /**
   * Getter for chat type.
   */
  get threadType(): string {
    return this._threadType;
  }

  /**
   * Getter for chat location.
   */
  get threadPath(): string {
    return this._threadPath;
  }

  /**
   * Getter for loading spinner toggle
   */
  get loading(): boolean {
    return this._loading;
  }

  /**
   * Getter for list of all chat participants, active and inactive.
   */
  get chatParticipants(): Person[] {
    return this._chatParticipants;
  }

  /**
   * Getter for loading status to be used with progress loading bar.
   */
  get loadingStatus(): number {
    return this._loadingStatus;
  }

  //endregion getters

  //region setters

  /**
   * Setter for loading status to be used with progress bar.
   * @param {number} value
   */
  set loadingStatus(value: number) {
    this._loadingStatus = value;
  }

  /**
   * Setter for all chat participants, active and inactive.
   * @param {Person[]} value
   */
  set chatParticipants(value: Person[]) {
    this._chatParticipants = value;
  }

  /**
   * Setter for parth of chat.
   * @param {string} threadPath
   */
  set threadPath(threadPath: string) {
    this._threadPath = threadPath;
  }

  /**
   * Setter for type of chat.
   * @param {string} threadType
   */
  set threadType(threadType: string) {
    this._threadType = threadType;
  }

  /**
   * Setter for if uploader is still participant in chat.
   * @param {boolean} isStillParticipant
   */
  set isStillParticipant(isStillParticipant: boolean) {
    this._isStillParticipant = isStillParticipant;
  }

  /**
   * Setter for name of the chat.
   * @param {string} titleName
   */
  set titleName(titleName: string) {
    this._titleName = titleName;
  }

  /**
   * Setter for list of messages in the chat.
   * @param {ChatMessage[]} messages
   */
  set messages(messages: ChatMessage[]) {
    this._messages = messages;
  }

  /**
   * Sett for list of participants in the chat, active only.
   * @param {Person[]} participants
   */
  set participants(participants: Person[]) {
    this._participants = participants;
  }

  //endregion setters

  //region toggles

  /**
   * Toggles the display of the full page loading spinner.
   */
  toggleSpinner() {
    this._loading = !this.loading;
  }

  //endregion toggles

  //region cross-component observers

  private updateCardsCallSource = new Subject<any>();
  updateCardsCalled$ = this.updateCardsCallSource.asObservable();

  /**
   * Call to update the cards to be displayed on the screen from data-upload to data-analysis.
   */
  updateCards() {
    this.updateCardsCallSource.next();
  }

  //endregion cross-component observers


}
