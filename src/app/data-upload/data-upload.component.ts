import {Component, Inject, OnInit} from '@angular/core';
import {DataService} from "../services/data-service.service";
import {DataAnalysisComponent} from "../data-analysis/data-analysis.component";
import {Data} from "@angular/router";

@Component({
  selector: 'app-data-upload',
  templateUrl: './data-upload.component.html',
  styleUrls: ['./data-upload.component.css']
})
export class DataUploadComponent implements OnInit {
  dataAnalysisComponent: DataAnalysisComponent;
  dataService: DataService;

  constructor(private ds: DataService) {
  this.dataService = ds;
  }

  ngOnInit() {
  }

  file: File;


  fileChange($event): void {
    this.dataService.toggleSpinner();
    this.readChatFile($event.target);
  }

  readChatFile(inputValue: any): void {
    let results;
    let file: File = inputValue.files[0];
    let myReader: FileReader = new FileReader();

    myReader.onloadend = (e)=> {
      results = JSON.parse(myReader.result);
      this.dataService.participants = results.participants;
      this.dataService.messages = results.messages;
      this.dataService.titleName = results.title;
      this.dataService.isStillParticipant = results.is_still_participant;
      this.dataService.threadType = results.thread_type;
      this.dataService.threadPath = results.thread_path;

      //TODO: remove these
      console.log(this.dataService.participants);
      console.log(this.dataService.messages);
      console.log(this.dataService.titleName);
      console.log(this.dataService.isStillParticipant);
      console.log(this.dataService.threadType);
      console.log(this.dataService.threadPath);

      this.updateCards();
      this.dataService.toggleSpinner();
    };

    myReader.readAsText(file);
  }

  updateCards() {
    this.dataService.updateCards();
  }


}
