import {Component, Input, OnInit} from '@angular/core';
import {Person} from "../interfaces/person";

@Component({
  selector: 'app-cards',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.setAMPDTT();
    this.autoCollapse();
  }

  @Input('cardData')
  data: Person;

  height: 0;
  cardData = {
    AMPDTT: null,
    isActive: null,
    isOpen: null
  };

  /**
   * Toggles whether a card is minimized or not.
   */
  toggleCollapse() {
    if(this.cardData.isOpen == null){
      this.cardData.isOpen = 'collapse in';
    } else {
      this.cardData.isOpen = null;
    }
  }

  /**
   * Set the message to display for average messages per day tool tip..
   */
  setAMPDTT(){
    this.cardData.AMPDTT = "Average messages per day are weighted by the users activity. Between the times of their first and last messages."
  }

  /**
   * If participant is inactive, auto collapse their card.
   */
  autoCollapse(){
    if(!this.data.active){
      this.toggleCollapse();
      this.cardData.isActive = 'card-inactive';
    }
  }
}


